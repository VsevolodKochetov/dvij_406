import React, {useEffect} from 'react';
import './App.css';

function App() {
  useEffect(() => { window.addEventListener('keydown', e => keydownHandler(e)) },[])

  let blocks = [
    [2,0,0,0],
    [0,2,0,0],
    [0,0,0,0],
    [0,0,0,0]
  ]
  
  const up = () => {
    blocks = blocks.reduce((acc, row, index) => {
      row.forEach((block, i) => {
        if (index === 0) return
        if (acc[index - 1][i] === block || acc[index - 1][i] === 0) acc[index][i] = acc[index - 1][i] + acc[index][i]
      })
    }, [...blocks])
  }

  const down = () => {}
  const left = () => {}
  const right = () => {}

  const keydownHandler = ({key}) => {
    switch(key){
      case 'ArrowUp': 
        up()
        break  
      case 'ArrowDown': 
        down()
        break  
      case 'ArrowLeft': 
        left()
        break  
      case 'ArrowRight': 
        right()
        break  
    }
  }

  const renderBlocks = () => {
    return blocks.map(row => {
      return <Row>
        {row.map(block => {
          return <Block value={block} />
        })}
      </Row>
    })
  }

  return (
    <div className="App">
      <p>Управление стрелками</p>
      <div className="game">
        {renderBlocks()}
      </div>
    </div>
  );
}


const Block = props => {
  return <div className='block'>{props.value}</div>
}

const Row = props => {
  return <div className='row'>{props.children}</div>
}

export default App;
